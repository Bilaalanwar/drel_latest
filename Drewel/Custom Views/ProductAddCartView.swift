//
//  ProductAddCartView.swift
//  Drewel
//
//  Created by Octal on 27/06/18.
//  Copyright © 2018 Octal. All rights reserved.
//

import UIKit

class ProductAddCartView: UIView {
    
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var btnAddToWishlist: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
